def radix_sort(A, n, d):
    # Function to perform a stable sort on array A based on the i-th digit
    def counting_sort_on_digit(A, digit):
        B = [0] * n
        k = 10  # Assuming digits are in the range 0-9

        C = [0] * k

        # Count the occurrences of each digit at position 'digit'
        for j in range(n):
            digit_value = (A[j] // (10 ** digit)) % 10
            C[digit_value] += 1

        # Accumulate the counts to get the positions of elements in the sorted output
        for i in range(1, k):
            C[i] += C[i - 1]

        # Copy elements from A to B based on their positions in the sorted order
        for j in range(n - 1, -1, -1):
            digit_value = (A[j] // (10 ** digit)) % 10
            B[C[digit_value] - 1] = A[j]
            C[digit_value] -= 1

        return B

    # Iterate through each digit (from least significant to most significant)
    for i in range(d):
        A = counting_sort_on_digit(A, i)

    return A

# Example usage:
A = [170, 45, 75, 90, 802, 24, 2, 66, 34, 14]
n = len(A)
d = 3  # Assuming all numbers have up to 3 digits

sorted_array = radix_sort(A, n, d)

print("Sorted Array:", sorted_array)
