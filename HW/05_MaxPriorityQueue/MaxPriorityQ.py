import heapq

class MaxPriorityQueue:
    def __init__(self, k):
        self.heap = []
        self.k = k

    def insert(self, x):
        if len(self.heap) < self.k:
            heapq.heappush(self.heap, x)
        elif x > self.heap[0]:
            heapq.heapreplace(self.heap, x)

    def delMax(self):
        if not self.isEmpty():
            return heapq.heappop(self.heap)
        return None

    def isEmpty(self):
        return len(self.heap) == 0

    def __repr__(self):
        return f"MaxPriorityQueue({self.heap})"

pq = MaxPriorityQueue(5)
pq.insert(45)
pq.insert(31)
pq.insert(14)
pq.insert(13)
pq.insert(20)
pq.insert(7)
pq.insert(11)
pq.insert(12)
pq.insert(7)

print("Priority queue:", pq)
print("Node with maximum priority:", pq.delMax())
print("Priority queue after extracting maximum:", pq)

pq.insert(49)

print("Priority queue after priority change:", pq)

pq.delMax()

print("Priority queue after removing the element:", pq)