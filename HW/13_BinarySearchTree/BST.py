class Node:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None
        self.parent = None

class BinarySearchTree:
    def __init__(self):
        self.root = None

    def tree_minimum(self, x):
        while x.left:
            x = x.left
        return x

    def tree_maximum(self, x):
        while x.right:
            x = x.right
        return x

    def tree_successor(self, x):
        if x.right:
            return self.tree_minimum(x.right)
        y = x.parent
        while y and x == y.right:
            x = y
            y = y.parent
        return y

    def tree_predecessor(self, x):
        if x.left:
            return self.tree_maximum(x.left)
        y = x.parent
        while y and x == y.left:
            x = y
            y = y.parent
        return y

    def tree_insert(self, key):
        new_node = Node(key)
        y = None
        x = self.root

        while x:
            y = x
            if key < x.key:
                x = x.left
            else:
                x = x.right

        new_node.parent = y

        if not y:
            self.root = new_node
        elif key < y.key:
            y.left = new_node
        else:
            y.right = new_node

    def transplant(self, u, v):
        if not u.parent:
            self.root = v
        elif u == u.parent.left:
            u.parent.left = v
        else:
            u.parent.right = v

        if v:
            v.parent = u.parent

    def tree_delete(self, key):
        z = self.search(key)

        if not z.left:
            self.transplant(z, z.right)
        elif not z.right:
            self.transplant(z, z.left)
        else:
            y = self.tree_minimum(z.right)
            if y.parent != z:
                self.transplant(y, y.right)
                y.right = z.right
                y.right.parent = y
            self.transplant(z, y)
            y.left = z.left
            y.left.parent = y

    def search(self, key):
        x = self.root
        while x and x.key != key:
            if key < x.key:
                x = x.left
            else:
                x = x.right
        return x

    def inorder_traversal(self, node, result=[]):
        if node:
            self.inorder_traversal(node.left, result)
            result.append(node.key)
            self.inorder_traversal(node.right, result)
        return result

# Example usage:
bst = BinarySearchTree()

# Inserting nodes
keys = [5, 3, 7, 2, 4, 6, 8]
for key in keys:
    bst.tree_insert(key)

# Inorder traversal
print("Inorder Traversal:", bst.inorder_traversal(bst.root))

# Minimum and Maximum
print("Minimum:", bst.tree_minimum(bst.root).key)
print("Maximum:", bst.tree_maximum(bst.root).key)

# Successor and Predecessor
node = bst.search(4)
print("Successor of 4:", bst.tree_successor(node).key)
print("Predecessor of 4:", bst.tree_predecessor(node).key)

# Delete a node
bst.tree_delete(5)
print("Inorder Traversal after deleting 5:", bst.inorder_traversal(bst.root))
