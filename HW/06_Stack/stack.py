def is_stack_empty(S):
    return S['top'] == 0

def push(S, x, size):
    if S['top'] == size:
        print("Error: Overflow")
    else:
        S['top'] += 1
        S[S['top']] = x

def pop(S):
    if is_stack_empty(S):
        print("Error: Underflow")
    else:
        S['top'] -= 1
        return S[S['top'] + 1]

# Example usage:

# Initialize a stack (assuming it's represented as a dictionary)
stack_size = 10
my_stack = {'top': 0}

# Check if the stack is empty
print("Is the stack empty?", is_stack_empty(my_stack))

# Push elements onto the stack
push(my_stack, 1, stack_size)
push(my_stack, 2, stack_size)
push(my_stack, 3, stack_size)

# Pop elements from the stack
popped_element = pop(my_stack)
print("Popped element:", popped_element)

# Check if the stack is empty after popping
print("Is the stack empty?", is_stack_empty(my_stack))