# define insersion sort method
def insertion_sort(A, n): 
    for i in range (1, n): 
        key = A[i]
        j = i-1
        while j>=0 and A[j]>key:
            A[j+1] = A[j]
            j = j-1
        A[j+1] = key
    return A

grades = [25, 12, 42, 90, 55, 48] # test array
insertion_sort(grades, 6)
print(grades)