class LinearProbingHashTable:
    def __init__(self, size):
        self.size = size
        self.table = [None] * size

    def hash_function(self, key, attempt):
        return (key + attempt) % self.size

    def hash_insert(self, key):
        i = 0
        while i < self.size:
            q = self.hash_function(key, i)
            if self.table[q] is None:
                self.table[q] = key
                return q  # Return the index where the key is inserted
            else:
                i += 1
        raise ValueError("Hash table overflow")

    def hash_search(self, key):
        i = 0
        while i < self.size:
            q = self.hash_function(key, i)
            if self.table[q] == key:
                return q  # Return the index where the key is found
            elif self.table[q] is None:
                return None  # Key not found
            else:
                i += 1
        return None  # Key not found

# Example usage:
hash_table_size = 15
linear_probing_hash_table = LinearProbingHashTable(hash_table_size)

# Inserting keys
index1 = linear_probing_hash_table.hash_insert(7)
index2 = linear_probing_hash_table.hash_insert(17)
index3 = linear_probing_hash_table.hash_insert(20)

# Searching for keys
result1 = linear_probing_hash_table.hash_search(17)
result2 = linear_probing_hash_table.hash_search(25)

print("Index of key 17:", result1)
print("Index of key 25:", result2)
