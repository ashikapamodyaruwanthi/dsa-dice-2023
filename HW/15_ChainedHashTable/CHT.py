class ChainedHashTable:
    def __init__(self, size):
        self.size = size
        self.table = [None] * size

    def hash_function(self, key):
        return key % self.size

    def chained_hash_insert(self, key, value):
        hash_value = self.hash_function(key)

        if self.table[hash_value] is None:
            self.table[hash_value] = []
        
        # Using a tuple (key, value) to represent an element in the list
        self.table[hash_value].append((key, value))

    def chained_hash_search(self, key):
        hash_value = self.hash_function(key)

        if self.table[hash_value] is not None:
            for item in self.table[hash_value]:
                if item[0] == key:
                    return item[1]  # Return the value associated with the key

        return None  # Key not found

    def chained_hash_delete(self, key):
        hash_value = self.hash_function(key)

        if self.table[hash_value] is not None:
            for item in self.table[hash_value]:
                if item[0] == key:
                    self.table[hash_value].remove(item)
                    return

# Example usage:
hash_table_size = 10
chained_hash_table = ChainedHashTable(hash_table_size)

# Inserting key-value pairs
chained_hash_table.chained_hash_insert(5, "Value1")
chained_hash_table.chained_hash_insert(15, "Value2")
chained_hash_table.chained_hash_insert(25, "Value3")

# Searching for a key
result = chained_hash_table.chained_hash_search(15)
print("Value at key 15:", result)

# Deleting a key
chained_hash_table.chained_hash_delete(15)
print("Value at key 15 after deletion:", chained_hash_table.chained_hash_search(15))
