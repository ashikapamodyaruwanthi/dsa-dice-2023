def merge_sort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2
        left_arr = arr[:mid]
        right_arr = arr[mid:]

        # Recursive calls
        left_arr = merge_sort(left_arr)
        right_arr = merge_sort(right_arr)

        # Merge the sorted subarrays
        merge(arr, left_arr, right_arr)

    return arr


def merge(arr, left_arr, right_arr):
    i = j = k = 0

    while i < len(left_arr) and j < len(right_arr):
        if left_arr[i] < right_arr[j]:
            arr[k] = left_arr[i]
            i += 1
        else:
            arr[k] = right_arr[j]
            j += 1
        k += 1

    while i < len(left_arr):
        arr[k] = left_arr[i]
        i += 1
        k += 1

    while j < len(right_arr):
        arr[k] = right_arr[j]
        j += 1
        k += 1



# Testing merge sort
test_arr = [3, 6, 8, 4, 6, 1, 2, 8, 9, 2, 4]
print('Test array: ' + str(test_arr))
merge_sort(test_arr)
print('Merge sorted array: ' + str(test_arr))