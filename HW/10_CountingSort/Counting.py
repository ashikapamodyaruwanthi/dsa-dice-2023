def counting_sort(A, n, k):
    B = [0] * n
    C = [0] * (k + 1)

    # Count the occurrences of each element in A
    for j in range(n):
        C[A[j]] += 1

    # Accumulate the counts to get the positions of elements in the sorted output
    for i in range(1, k + 1):
        C[i] += C[i - 1]

    # Copy elements from A to B based on their positions in the sorted order
    for j in range(n - 1, -1, -1):
        B[C[A[j]] - 1] = A[j]
        C[A[j]] -= 1

    return B

# Example usage:
A = [2, 5, 3, 0, 2, 3, 0, 3]
n = len(A)
k = max(A)
sorted_array = counting_sort(A, n, k)

print("Sorted Array:", sorted_array)
