import random

def randomized_partition(A, p, r):
    i = random.randint(p, r)
    A[r], A[i] = A[i], A[r]  # exchange A[r] with A[i]
    return partition(A, p, r)

def partition(A, p, r):
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i += 1
            A[i], A[j] = A[j], A[i]
    A[i + 1], A[r] = A[r], A[i + 1]
    return i + 1

def randomized_quicksort(A, p, r):
    if p < r:
        q = randomized_partition(A, p, r)
        randomized_quicksort(A, p, q - 1)
        randomized_quicksort(A, q + 1, r)

# Example usage:

# Initialize an array (list) for testing
my_array = [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5]

# Perform randomized quicksort on the array
randomized_quicksort(my_array, 0, len(my_array) - 1)

# Print the sorted array
print("Sorted array:", my_array)