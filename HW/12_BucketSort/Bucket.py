def bucket_sort(A):
    n = len(A)

    # Create an array of empty lists
    B = [[] for _ in range(n)]

    # Distribute elements into buckets
    for i in range(n):
        index = int(n * A[i])
        B[index].append(A[i])

    # Sort each bucket using insertion sort
    for i in range(n):
        B[i].sort()

    # Concatenate the lists together in order
    sorted_array = [element for bucket in B for element in bucket]

    return sorted_array

# Example usage:
A = [0.78, 0.17, 0.39, 0.26, 0.72, 0.94, 0.21, 0.12, 0.23]
sorted_array = bucket_sort(A)

print("Sorted Array:", sorted_array)
