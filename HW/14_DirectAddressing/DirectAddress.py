class DirectAddressTable:
    def __init__(self, size):
        self.table = [None] * size

    def direct_address_search(self, key):
        return self.table[key]

    def direct_address_insert(self, key, value):
        self.table[key] = value

    def direct_address_delete(self, key):
        self.table[key] = None

# Example usage:
table_size = 100
direct_address_table = DirectAddressTable(table_size)

# Inserting a key-value pair
direct_address_table.direct_address_insert(5, "SomeValue")

# Searching for a key
result = direct_address_table.direct_address_search(5)
print("Value at key 5:", result)

# Deleting a key
direct_address_table.direct_address_delete(5)
print("Value at key 5 after deletion:", direct_address_table.direct_address_search(5))
