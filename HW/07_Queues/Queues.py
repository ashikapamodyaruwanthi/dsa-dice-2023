def enqueue(Q, x, size):
    Q['tail'] = x
    if Q['tail'] == size:
        Q['tail'] = 1
    else:
        Q['tail'] += 1

def dequeue(Q):
    x = Q['head']
    if Q['head'] == Q['size']:
        Q['head'] = 1
    else:
        Q['head'] += 1
    return x

# Example usage:

# Initialize a queue (assuming it's represented as a dictionary)
queue_size = 10
my_queue = {'head': 1, 'tail': 1, 'size': queue_size}

# Enqueue elements into the queue
enqueue(my_queue, 1, queue_size)
enqueue(my_queue, 2, queue_size)
enqueue(my_queue, 3, queue_size)

# Dequeue elements from the queue
dequeued_element = dequeue(my_queue)
print("Dequeued element:", dequeued_element)
