package oy.tol.tra;

import java.util.Arrays;

/**
 * An implementation of the StackInterface.
 * <p>
 * TODO: Students, implement this so that the tests pass.
 * 
 * Note that you need to implement construtor(s) for your concrete StackImplementation, which
 * allocates the internal Object array for the Stack:
 * - a default constructor, calling the StackImplementation(int size) with value of 10.
 * - StackImplementation(int size), which allocates an array of Object's with size.
 *  -- remember to maintain the capacity and/or currentIndex when the stack is manipulated.
 */
public class StackImplementation<E> implements StackInterface<E> {

   // TODO: Add member variables needed.
   // Do not use constant values in code, e.g. 10. Instead, define a constant for that. For example:
   // private static final int MY_CONSTANT_VARIABLE = 10;
      private static final int DEFAULT_CAPACITY = 10;
      private Object[] itemArray;
      private int size;

   /**
    * Allocates a stack with a default capacity.
    * @throws StackAllocationException
    */
   public StackImplementation() throws StackAllocationException {
      // TODO: call the constructor with size parameter with default size (see member variable!).
      this(DEFAULT_CAPACITY);
   }

   /** TODO: Implement so that
    * - if the size is less than 2, throw StackAllocationException
    * - if the allocation of the array throws with Java exception,
    *   throw StackAllocationException.
    * @param capacity The capacity of the stack.
    * @throws StackAllocationException If cannot allocate room for the internal array.
    */
   public StackImplementation(int capacity) throws StackAllocationException {
      if (capacity < 2) {
         throw new StackAllocationException("Stack capacity must be at least 2");
     }
     try {
         itemArray = new Object[capacity];
     } catch (Exception e) {
         throw new StackAllocationException("Failed to allocate stack array");
     }
   }

   @Override
   public int capacity() {
      // TODO: Implement this
      return itemArray.length;
   }

   @Override
   public void push(E element) throws StackAllocationException, NullPointerException {
      // TODO: Implement this
      if (element == null) {
         throw new NullPointerException("Cannot push null element onto the stack");
     }
     ensureCapacity();
     itemArray[size++] = element;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E pop() throws StackIsEmptyException {
      // TODO: Implement this
      if (isEmpty()) {
         throw new StackIsEmptyException("Cannot pop from an empty stack");
     }
     E poppedElement = (E) itemArray[--size];
     itemArray[size] = null; // Dereference to allow garbage collection
     return poppedElement;

   }

   @SuppressWarnings("unchecked")
   @Override
   public E peek() throws StackIsEmptyException {
      // TODO: Implement this
      if (isEmpty()) {
         throw new StackIsEmptyException("Cannot peek into an empty stack");
     }
     return (E) itemArray[size - 1];
   }

   @Override
   public int size() {
      // TODO: Implement this
      return size;
   }
   @Override
   public void clear() {
      // TODO: Implement this
              Arrays.fill(itemArray, null);
        size = 0;
   }

   @Override
   public boolean isEmpty() {
      // TODO: Implement this
      return size == 0;
   }

   @Override
   public String toString() {
      // TODO: Implement this
      StringBuilder result = new StringBuilder("[");
      for (int i = 0; i < size; i++) {
          result.append(itemArray[i]);
          if (i < size - 1) {
              result.append(", ");
          }
      }
      result.append("]");
      return result.toString();
   }

   private void ensureCapacity() throws StackAllocationException {
      if (size == itemArray.length) {
          itemArray = Arrays.copyOf(itemArray, size * 2);
      }
  }
}
