# Learnings from the course tasks and reports

Write a couple of sentences per task, what did you learn from the task, how did you manage to do it.

If a task asks you to **report** something, do write the report in this document.

## 00-init

## 01-arrays

## 02-mode

The time complexity of the sorting algorithm is likely O(n^2) due to the use of insertion sort. The execution time has been increased with array size. After sorting, the code iterates through the sorted array once to identify the mode. The time complexity of findmode is O(n).   
The mode finding part, which is after sorting, involves a linear scan through the sorted array. Therefore, it contributes to the overall time complexity, but it's likely not the dominant factor.

## 03-draw


## 04-1-stack


## 04-2-queue


## 04-3-linkedlist


## 05-binsearch


## 05-invoices


## 67-phonebook

There is a report related to this task. Write that here!

## Optional tasks

If you did any optional tasks, list them here so that teachers will check them out and grade them!


# Feedback section

General feedback and costructive development ideas for the course, please!