package oy.tol.tra;

public class Algorithms {


    public static class ModeSearchResult<T> {
        public T theMode;
        public int count = 0;
    }

    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();

        // Check for null or insufficient array size
        if (array == null || array.length < 2) {
            result.theMode = null;
            result.count = -1;
            return result;
        }

        // Sort the array
        sort(array);

        T currentMode = array[0];
        T tempMode = array[0];
        int currentCount = 1;
        int maxCount = 1;

        for (int i = 1; i < array.length; i++) {
            if (array[i].equals(array[i - 1])) {
                currentCount++;
            } else {
                currentCount = 1;
                tempMode = array[i];
            }

            if (currentCount > maxCount) {
                maxCount = currentCount;
                currentMode = tempMode;
            }
        }

        result.theMode = currentMode;
        result.count = maxCount;

        return result;
    }

        public static <T extends Comparable<T>> void sort(T[] array) {
        for (int i = 1; i < array.length; ++i) {
            T key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j].compareTo(key) > 0) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }
  
}