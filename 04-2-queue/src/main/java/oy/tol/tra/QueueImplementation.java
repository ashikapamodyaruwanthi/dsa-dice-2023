package oy.tol.tra;

public class QueueImplementation<E> implements QueueInterface<E> {

    private Object[] itemArray;
    private int head;
    private int tail;
    private int size;
    private static final int DEFAULT_CAPACITY = 10;

        /**
     * Default constructor to initialize the queue with a default capacity.
     */
    public QueueImplementation() {
        this(DEFAULT_CAPACITY);
    }

    public QueueImplementation(int INITIAL_CAPACITY) {
        if (INITIAL_CAPACITY <= 0) {
            throw new IllegalArgumentException("Capacity must be greater than zero");
        }
        this.itemArray = new Object[INITIAL_CAPACITY];
        this.head = 0;
        this.tail = 0;
        this.size = 0;
    }

    

    @Override
    public int capacity() {
        return itemArray.length;
    }

    @Override
    public void enqueue(E element) throws QueueAllocationException, NullPointerException {
        if (element == null) {
            throw new NullPointerException("Element cannot be null");
        }

        if (size == itemArray.length) {
            // Resize the array if it's full
            resizeArray(itemArray.length * 2);
        }

        itemArray[tail] = element;
        tail = (tail + 1) % itemArray.length; // Circular increment
        size++;
    }

    @Override
    public E dequeue() throws QueueIsEmptyException {
        if (isEmpty()) {
            throw new QueueIsEmptyException("Queue is empty");
        }

        E dequeuedElement = (E) itemArray[head];
        itemArray[head] = null; // Clearing the reference
        head = (head + 1) % itemArray.length; // Circular increment
        size--;

        return dequeuedElement;
    }

    @Override
    public E element() throws QueueIsEmptyException {
        if (isEmpty()) {
            throw new QueueIsEmptyException("Queue is empty");
        }
        return (E) itemArray[head];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            itemArray[(head + i) % itemArray.length] = null;
        }
        head = 0;
        tail = 0;
        size = 0;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            result.append(itemArray[(head + i) % itemArray.length]);
            if (i < size - 1) {
                result.append(", ");
            }
        }
        result.append("]");
        return result.toString();
    }

    // Private method to resize the array
    private void resizeArray(int newCapacity) {
        Object[] newArray = new Object[newCapacity];
        for (int i = 0; i < size; i++) {
            newArray[i] = itemArray[(head + i) % itemArray.length];
        }
        itemArray = newArray;
        head = 0;
        tail = size;
    }
}
