package oy.tol.tra;

import java.util.Arrays;

public class Algorithms {
    
     
    
    public static <T extends Comparable<T>> int binarySearch(T aValue, T[] fromArray, int fromIndex, int toIndex) {
        // Ensure the array is sorted before performing binary search

        while (fromIndex <= toIndex) {
            int mid = (fromIndex + toIndex) / 2;
            int cmp = fromArray[mid].compareTo(aValue);
            if (cmp < 0) {
                fromIndex = mid + 1;
            } else if (cmp > 0) {
                toIndex = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
}
 // Slow sort algorithm
    public static <T extends Comparable<T>> void sort(T[] array) {
        for (int i = 1; i < array.length; ++i) {
            T key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j].compareTo(key) > 0) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }

     public static <E extends Comparable<E>> void fastSort(Comparable<E>[] array) {
        mergeSort(array);
    }

    private static <E extends Comparable<E>> void mergeSort(Comparable<E>[] array) {
        if (array.length > 1) {
            int mid = array.length / 2;
            Comparable<E>[] left = Arrays.copyOfRange(array, 0, mid);
            Comparable<E>[] right = Arrays.copyOfRange(array, mid, array.length);

            mergeSort(left);
            mergeSort(right);

            merge(array, left, right);
        }
    }

    private static <E extends Comparable<E>> void merge(Comparable<E>[] array, Comparable<E>[] left, Comparable<E>[] right) {
        int i = 0, j = 0, k = 0;

        while (i < left.length && j < right.length) {
            if (left[i].compareTo((E) right[j]) <= 0) {
                array[k++] = left[i++];
            } else {
                array[k++] = right[j++];
            }
        }

        while (i < left.length) {
            array[k++] = left[i++];
        }

        while (j < right.length) {
            array[k++] = right[j++];
        }
    }
}