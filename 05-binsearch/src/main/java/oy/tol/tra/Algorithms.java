package oy.tol.tra;

public class Algorithms {
    
     
    
    public static <T extends Comparable<T>> int binarySearch(T aValue, T[] fromArray, int fromIndex, int toIndex) {
        // Ensure the array is sorted before performing binary search

        while (fromIndex <= toIndex) {
            int mid = (fromIndex + toIndex) / 2;
            int cmp = fromArray[mid].compareTo(aValue);
            if (cmp < 0) {
                fromIndex = mid + 1;
            } else if (cmp > 0) {
                toIndex = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
}
 // to make ascending order
    public static <T extends Comparable<T>> void sort(T[] array) {
        for (int i = 1; i < array.length; ++i) {
            T key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j].compareTo(key) > 0) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }
}