package oy.tol.tra;
import java.util.function.Predicate;

public class Algorithms {


        // Other methods in Algorithms class...
    
        public static <T> int partitionByRule(T[] array, int count, Predicate<T> rule) {
            int index = 0;
            while (index < count && !rule.test(array[index])) {
                index++;
            }
    
            if (index >= count) {
                return count; // No conforming elements in the array, return the end
            }
    
            int nextIndex = index + 1;
            while (nextIndex < count) {
                if (!rule.test(array[nextIndex])) {
                    swap(array, index, nextIndex);
                    index++;
                }
                nextIndex++;
            }
    
            return index;
        }
    
        // Generic swap method
        public static <T> void swap(T[] array, int i, int j) {
            T temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
  