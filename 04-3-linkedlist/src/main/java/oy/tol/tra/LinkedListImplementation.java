package oy.tol.tra;

import org.w3c.dom.Node;

public class LinkedListImplementation<E> implements LinkedListInterface<E> {

   private class Node<T> {

      Node(T data) {
         element = data;
         next = null;
      }
      T element;
      Node<T> next;

      @Override
      public String toString() {
         return element.toString();
      }
   }

   private Node<E> head = null;
   private int size = 0;

   @Override
   public void add(E element) throws NullPointerException, LinkedListAllocationException {
      // TODO: Implement this.
      if (element == null) {
         throw new NullPointerException("Element cannot be null");
      }
      Node<E> newNode = new Node<>(element);
      if (head == null) {
         head = newNode;
      } else {
         Node<E> current = head;
         while (current.next != null) {
            current = current.next;
         }
         current.next = newNode;
      }
      size++;
   }

   @Override
   public void add(int index, E element) throws NullPointerException, LinkedListAllocationException, IndexOutOfBoundsException {
      // TODO: Implement this.
      if (element == null) {
         throw new NullPointerException("Element cannot be null");
      }
      if (index < 0 || index > size) {
         throw new IndexOutOfBoundsException("Index out of bounds");
      }
      Node<E> newNode = new Node<>(element);
      if (index == 0) {
         newNode.next = head;
         head = newNode;
      } else {
         Node<E> current = head;
         for (int i = 0; i < index - 1; i++) {
            current = current.next;
         }
         newNode.next = current.next;
         current.next = newNode;
      }
      size++;
   }

   @Override
   public boolean remove(E element) throws NullPointerException {
      // TODO: Implement this.
      if (element == null) {
         throw new NullPointerException("Element cannot be null");
      }
      if (head == null) {
         return false;
      }
      if (head.element.equals(element)) {
         head = head.next;
         size--;
         return true;
      }
      Node<E> current = head;
      while (current.next != null && !current.next.element.equals(element)) {
         current = current.next;
      }
      if (current.next != null) {
         current.next = current.next.next;
         size--;
         return true;
      }
      return false;
   }

   @Override
   public E remove(int index) throws IndexOutOfBoundsException {
      // TODO: Implement this.
      if (index < 0 || index >= size) {
         throw new IndexOutOfBoundsException("Index out of bounds");
      }
      E removedElement;
      if (index == 0) {
         removedElement = head.element;
         head = head.next;
      } else {
         Node<E> current = head;
         for (int i = 0; i < index - 1; i++) {
            current = current.next;
         }
         removedElement = current.next.element;
         current.next = current.next.next;
      }
      size--;
      return removedElement;
   }

   @Override
   public E get(int index) throws IndexOutOfBoundsException {
      // TODO: Implement this.
      if (index < 0 || index >= size) {
         throw new IndexOutOfBoundsException("Index out of bounds");
      }
      Node<E> current = head;
      for (int i = 0; i < index; i++) {
         current = current.next;
      }
      return current.element;
   }

   @Override
   public int indexOf(E element) throws NullPointerException {
      // TODO: Implement this.
      if (element == null) {
         throw new NullPointerException("Element cannot be null");
      }
      Node<E> current = head;
      for (int i = 0; i < size; i++) {
         if (current.element.equals(element)) {
            return i;
         }
         current = current.next;
      }
      return -1;
   }

   @Override
   public int size() {
      // TODO: Implement this.
      return size;
   }

   @Override
   public void clear() {
      // TODO: Implement this.
      head = null;
      size = 0;
   }

   @Override
   public void reverse() {
      // TODO: implement this only when doing the task explained the TASK-2.md.
      // This method is not needed in doing the task in the README.md.
   Node<E> prev = null;
   Node<E> current = head;
   Node<E> next = null;

   while (current != null) {
      next = current.next;
      current.next = prev;
      prev = current;
      current = next;
   }

   head = prev;
   }

   @Override
   public String toString() {
      // TODO: Implement this.
      StringBuilder result = new StringBuilder("[");
   Node<E> current = head;
   while (current != null) {
      result.append(current.element);
      if (current.next != null) {
         result.append(", ");
      }
      current = current.next;
   }
   result.append("]");
   return result.toString();
   }
   
}
